create table Vehicles (
    licensenum varchar(7),
    color varchar(8),
    size enum('small', 'medium', 'large')
    primary key(licensenum)
);

create table ParkingSpace (
    levelnum enum('0','1','2','3','4'),
    spaceid enum('0','1','2','3','4','5','6','7','8','9')
    lnum varchar(7),
    foreign key(lnum) references Vehicles(licensenum)
);