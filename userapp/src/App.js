import './App.css';
import React from 'react';
import axios from 'axios';

export default class App extends React.Component {

  state = {
    phoneno: '',
    level: '',
    space: '',
    submit: false,
    pherr: '',
    wait: false,
    resp: '',
    status: 'Form not submitted'
  }

  handleChange = (evt) => {
    evt.preventDefault();
    this.setState({[evt.target.name]: evt.target.value});
  }


  testAPI = async event => {
    if (this.state.phoneno === '' || this.state.phoneno.length !== 10) {
      this.setState({pherr: 'Please enter a valid phone number'});
      return
    }
    console.log(this.state.phoneno);
    // clear error message
    this.setState({pherr: ''});
    this.setState({submit: true});
    const send = {
      phoneno: this.state.phoneno,
      level: this.state.level,
      space: this.state.space
    }
    await axios.post(`http://localhost:5000/getPhone`, { send })
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({
          resp: res.data
        });
      })
      console.log('here')
    if (this.state.resp === 'Thank you for parking, form submitted!') {
      let str = 'Parked at Level: ' + this.state.level + ' Space: ' + this.state.space;
      this.setState({
        status: str
      });
      await axios.post(`http://localhost:5000/updateConnection`, { send })
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({
          status: res.data
        });
      })
    }
      //
  //     while (this.state.wait === false) {
  //       this.updateConnection();
  //     }
  }



  render() {
    return(
      <div className="App">
        <h1>Please fill out the following</h1>
        <div className="inpt-group">
          <input type="text" name="phoneno" placeholder="Please enter a phone number" onChange={this.handleChange}/>
          <div>{this.state.pherr}</div>
        </div>
        <div className="wrapper">
              <div className="first">
              <h4>Select level</h4>
                <select id="dropdown" name="level" onChange={this.handleChange}>
                <option value="">Select level</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
              </div>
              <div className="second">
              <h4>Select space number</h4>
              <select id="dropdown" name="space" onChange={this.handleChange}>
                <option value="">Select space</option>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
              </select>
              </div>
              <div className ="divider"/>
              {this.state.resp}
            </div>
            <div className ="divider"/>
            <div className="btn-group">
            <button onClick= {this.testAPI.bind(this)} type="button">Submit</button>
            </div>
            <div className ="divider"/>
            <div>
              <b><p>Status:</p></b>
              <i>{this.state.status}</i>
            </div>
      </div>
    );
  }
};

