import './App.css';
import React from 'react';
import axios from 'axios';



export default class App extends React.Component {

  state = {
    red: '',
    green: '',
    blue: '',
    size: '',
    license: '',
    color: '',
    level:'',
    space:'',
    resp: '',
    err1: '',
    err2: ''
  }

  handleChange = (evt) => {
    evt.preventDefault();
    this.setState({[evt.target.name]: evt.target.value});
  }

  // add vehicle to Vehicle table, simulating a car entering the garage
  entersGarage = async event => {
    event.preventDefault();
    const send = {
      red: this.state.red,
      green: this.state.green,
      blue: this.state.blue,
      size: this.state.size,
      license: this.state.license
    }
    console.log(send)
    await axios.post(`http://localhost:5000/enterGarage`, { send })
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({
          resp: res.data
       });
       this.setState({
        err1: ''
       });
      })
    if (this.state.resp === 'Entry exists') {
      this.setState({err1: 'Vehicle already exists'});
    }
    else if (this.state.resp === 'Input error') {
      this.setState({err1: 'Input error, please generate a Vehicle'});
    }
  }
  // removes vehicle from Vehicle table, simulating a car exiting the garage
  exitsGarage = async event => {
    event.preventDefault();
    const send = {
      red: this.state.red,
      green: this.state.green,
      blue: this.state.blue,
      size: this.state.size,
      license: this.state.license
    }
    console.log(send)
    await axios.post(`http://localhost:5000/exitGarage`, { send })
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({
          resp: res.data
       })
      })
  }

  pullsIntoSpace = async event => {
    event.preventDefault();
    const send = {
      red: this.state.red,
      green: this.state.green,
      blue: this.state.blue,
      size: this.state.size,
      license: this.state.license,
      level: this.state.level,
      space: this.state.space
    }
    console.log(send)
    await axios.post(`http://localhost:5000/pullIntoSpace`, { send })
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({
          resp: res.data
       });
       this.setState({
        err2: ''
       });
      })
    if (this.state.resp === 'Entry exists') {
      this.setState({err2: 'License Plate # already exists in database, please generate a new vehicle'});
    }
    else if (this.state.resp === 'Input error') {
      this.setState({err2: 'Input error, please enter a valid level and/or space number'});

    }
  }

  pullsOutOfSpace = async event => {
    event.preventDefault();
    const send = {
      red: this.state.red,
      green: this.state.green,
      blue: this.state.blue,
      size: this.state.size,
      license: this.state.license
    }
    console.log(send)
    await axios.post(`http://localhost:5000/pullOutOfSpace`, { send })
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({
          resp: res.data
       })
      })
  }

  // generate a random vehicle to insert to Vehicle table
  vehicleGenerator() {
    let sizes = ['small', 'medium', 'large'];
    let licenserand = this.makeID(7);
    let redrand = Math.floor(Math.random() * 256);
    let bluerand = Math.floor(Math.random() * 256);
    let greenrand = Math.floor(Math.random() * 256);
    const sizerand = sizes[Math.floor(Math.random() * sizes.length)];
    let colordisp = String(redrand) + ', ' + String(greenrand) + ', ' + String(bluerand);

    this.setState({red: redrand, blue: bluerand, green: greenrand, size: sizerand, license: licenserand, color: colordisp});
    console.log(this.state);
  }
  // creates an alphanumeric license plate
  makeID = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (var i = 0; i < length; ++i) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
   }
   return result;
  }

  render() {
    return(
      <div className="App">
        <div>
          <h1>
            Sensor Options
          </h1>
        </div>
        <div>
          <h3>Click to generate a random vehicle</h3>
            <div className = 'btn-group2'>
            <button onClick = {this.vehicleGenerator.bind(this)} type="button">Generate</button>
            <p>License Number: {this.state.license}</p>
            <p>Size: {this.state.size}</p>
            <p>Color: {this.state.color}</p>
            {/* <div className="divider"/> */}
            </div>
        </div>
      
        <div className="divider"/>
          <div className = "btn-group">
            <button onClick= {this.entersGarage.bind(this)} type="button">Vehicle enters garage</button>
            <div>{this.state.err1}</div>
            <div className="divider"/>
            <button onClick= {this.exitsGarage.bind(this)} type="button">Vehicle exits garage</button>
            <div className ="divider"/>
            <button onClick= {this.pullsIntoSpace.bind(this)} type="button">Vehicle pulls into space</button>
            <div>
            <div className="wrapper">
              <div className="first">
              <h4>Select level</h4>
                <select id="dropdown" name="level" onChange={this.handleChange}>
                <option value="">Select level</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
              </div>
              <div className="second">
              <h4>Select space number</h4>
              <select id="dropdown" name="space" onChange={this.handleChange}>
                <option value="">Select space</option>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
              </select>
              </div>
            </div>
            <div>{this.state.err2}</div>
          </div>
            <div className ="divider"/>
            <button onClick= {this.pullsOutOfSpace.bind(this)} type="button">Vehicle pulls out of space</button>
          </div>
      </div>
    );
  }
}

