from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_sqlalchemy import sqlalchemy
import json

app = Flask(__name__)
CORS(app)


host = 'parking-database.csj1mipajfew.us-west-1.rds.amazonaws.com'
username = 'admin'
password = 'password'
dbname = 'parking-database'
port = '3306'
schema = 'default'

engine = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}:{3}/{4}'.format(username, password, host, port, schema))


@app.route('/updateConnection', methods = ['POST'])
def updateConnection():
    m = request.get_json()
    x = json.dumps(m)
    y = json.loads(x)
    level = '\'' + str((y["send"]["level"])) + '\' and spaceid = '
    space = '\'' + str((y["send"]["space"])) + '\');'
    # Select 1 from ParkingSpace where (levelid = and spaceid = );
    while True:
        with engine.connect() as conn:
            chk = conn.execute('SELECT 1 from ParkingSpace where (levelid = ' + level + space)
            if chk.fetchone():
                print('Entry exists')
            else:
                conn.close()
                break 
    return 'Vehicle has left the parking space, thank you for parking'

@app.route('/getPhone', methods = ['POST'])
def getPhone():
    m = request.get_json()
    x = json.dumps(m)
    y = json.loads(x)
    level = '\'' + str((y["send"]["level"])) + '\' and spaceid = '
    space = '\'' + str((y["send"]["space"])) + '\');'
    # Select 1 from ParkingSpace where (levelid = and spaceid = );
    with engine.connect() as conn:
        chk = conn.execute('SELECT 1 from ParkingSpace where (levelid = ' + level + space)
        if chk.fetchone():
            print('Entry exists')
            return 'Thank you for parking, form submitted!'
        print(chk)
        conn.close()
    return 'No vehicle parked at space, please enter the correct space/level number'


@app.route('/enterGarage', methods = ['POST'])
# adds a vehicle into the Vehicle table, simulating a car entering the structure
def enterGarage():
    m = request.get_json()
    x = json.dumps(m)
    y = json.loads(x)
    licensenum = '\'' + str((y["send"]["license"])) + '\','
    size = '\'' + str((y["send"]["size"])) + '\','
    r = '\'' + str((y["send"]["red"])) + '\','
    g = '\'' + str((y["send"]["green"])) + '\','
    b = '\'' + str((y["send"]["blue"]))+ '\');'
    # error checking
    if licensenum == '\'\',':
        print('Input error')
        return 'Input error'
    with engine.connect() as conn:
        # check if vehicle with the given license plate # already exists in the database
        lnchk = licensenum[:-1] + ';'
        print(lnchk)
        chk = conn.execute('SELECT 1 FROM Vehicle WHERE licensenum = ' + lnchk)
        # check if query has an output
        if chk.fetchone():
            print('Entry exists')
            return 'Entry exists'
        
        # execute valid query after error checking
        result = conn.execute('INSERT into Vehicle VALUES (' + licensenum + size + r + g + b)
        print(result)
        conn.close()
    return 'Success'

@app.route('/exitGarage', methods = ['POST'])
# removes a vehicle from the Vehicle table, simulating a car leaving the structure
def exitGarage():
    m = request.get_json()
    x = json.dumps(m)
    y = json.loads(x)
    licensenum = '\'' + str((y["send"]["license"])) + '\';'
    with engine.connect() as conn:
        result = conn.execute('DELETE FROM Vehicle where licensenum =' + licensenum)
        print(result)
        conn.close()
    return 'Success'

# adds a row into the ParkingSpace table, simulating a car entering a parking space
@app.route('/pullIntoSpace', methods = ['POST'])
def pullsIntoSpace():
    m = request.get_json()
    x = json.dumps(m)
    y = json.loads(x)
    licensenum = '\'' + str((y["send"]["license"])) + '\','
    level = '\'' + str((y["send"]["level"])) + '\','
    space = '\'' + str((y["send"]["space"])) + '\');'
    # error checking
    print(licensenum)
    print(level)
    print(space)
    # variables to check whether level and space is valid
    if licensenum == '\'\',' or level == '\'\',' or space == '\'\');':
        print('Input error')
        return 'Input error'

    with engine.connect() as conn:
        # check if vehicle with the given license plate # already exists in the database
        lnchk = licensenum[:-1] + ';'
        print(lnchk)
        chk = conn.execute('SELECT 1 FROM ParkingSpace WHERE lnum = ' + lnchk)
        # check if query has an output
        if chk.fetchone():
            print('Entry exists')
            return 'Entry exists'

        # execute valid query after error checking
        result = conn.execute('INSERT into ParkingSpace (lnum, levelid, spaceid) VALUES (' + licensenum + level + space)
        print(result)
        conn.close()
    return 'Success'

# removes a row from the ParkingSpace table, simulating a car leaving a parking space
@app.route('/pullOutOfSpace', methods = ['POST'])
def pullOutOfSpace():
    m = request.get_json()
    x = json.dumps(m)
    y = json.loads(x)
    licensenum = '\'' + str((y["send"]["license"])) + '\';'
    with engine.connect() as conn:
        result = conn.execute('DELETE FROM ParkingSpace where lnum =' + licensenum)
        print(result)
        conn.close()
    return 'Success'




if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000, debug=True)
