# Avid Parking Project



## Summary
This repository is an implementation of the parking structure project given by Avid.

## Technologies
This project is built on a Flask (Python) backend, which handles post requests from the 2 frontend apps, the User and Sensor application, which are built from React. This project utilizes the REST API to handle requests from the sensor/user, in order to run queries to the database. The database is ran on an AWS server, and is a MySQL server.

## Sensor Application
The sensor application generates a vehicle, and simulates the information a sensor would send to the backend with labeled buttons.

## User Application
The user application takes in a level & space id, and a phone number. The application then gives updates to the user when the person enters and leaves the parking space.
